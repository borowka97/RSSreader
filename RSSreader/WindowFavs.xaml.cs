﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel.Syndication;
using System.Text;
using System.Threading.Tasks;
using System.Web.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml;

namespace RSSreader
{
    /// <summary>
    /// Interaction logic for WindowFavs.xaml
    /// </summary>
    public partial class WindowFavs : Window
    {
        private readonly MainWindow _mainWindow;
        private KeyValueConfigurationCollection settings;

        public WindowFavs(MainWindow mainWindow)
        {
            _mainWindow = mainWindow;
            InitializeComponent();
            LoadFavs();
        }

        void LoadFavs()
        {
            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            
            settings = config.AppSettings.Settings;

            foreach (var itemSetting in settings.AllKeys)
            {
                ListBoxFavs.Items.Add(itemSetting);
            }
           
        }

        private void ButtonOpen_Click(object sender, RoutedEventArgs e)
        {
           _mainWindow.AddFeed(ListBoxFavs.SelectedItem.ToString());
            
        }

        private void ButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                settings = config.AppSettings.Settings;

                var link = LinkTextBox.Text;

                if (settings.AllKeys.Contains(link))
                {
                    MessageBox.Show("That link exist in favourites");
                    return;
                }


                var reader = XmlReader.Create(link);
                var feed = SyndicationFeed.Load(reader);
                var feedLink = feed.Links[0].Uri.ToString();

                settings.Add(feedLink, "");
                config.Save(ConfigurationSaveMode.Full);
                ConfigurationManager.RefreshSection(config.AppSettings.Settings.ToString());
                MessageBox.Show($"Feed has been added to favs!\n{feed.Title.Text}\n{feed.Links[0].Uri.AbsoluteUri}");
            }
            catch (Exception ex)
            {
                MessageBox.Show("URL is incorrect " + ex.Message);
            }

           
        }

        private void ButtonDelete_Click(object sender, RoutedEventArgs e)
        {
            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var link = ListBoxFavs.SelectedItem.ToString();
            settings = config.AppSettings.Settings;

            settings.Remove(link);
            ListBoxFavs.Items.Remove(link);

            config.Save(ConfigurationSaveMode.Full);
            ConfigurationManager.RefreshSection(config.AppSettings.Settings.ToString());
        }
    }
}
