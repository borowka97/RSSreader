﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace RSSreader
{
    class UrlCorrectnessRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            string url;

            url = value.ToString();

            if (Uri.IsWellFormedUriString(url, UriKind.RelativeOrAbsolute))
                return new ValidationResult(true, null);

            return new ValidationResult(false, "Link do RSS jest niepoprawny!");

        }
    }
    
}
