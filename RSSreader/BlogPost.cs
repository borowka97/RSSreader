﻿using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.ServiceModel.Syndication;

namespace RSSreader
{
    internal class BlogPost
    {
        private String _title, _body;
        private Uri _url;
        public BlogPost()
        {

        }
        public BlogPost(SyndicationItem item)
        {
            Title = item.Title.Text;
            Body = item.Summary.Text;
            Url = item.Links[0].Uri;
        }

        public string Title { get => _title; set => _title = value; }
        public string Body { get => _body; set => _body = value; }
        public Uri Url { get => _url; set => _url = value; }

        public override string ToString()
        {
            return this.Title.ToString();
        }
    }
    
}