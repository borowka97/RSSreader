﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.ComponentModel;
using System.ServiceModel.Syndication;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Navigation;
using System.Xml;
using System.Configuration;
using System.Linq;

namespace RSSreader
{
    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : INotifyPropertyChanged
    {
        private string _url;
        private int _tabsCounter = 1;
        private bool _navigation = true;
        //otwarte feedy klucz: link, wartosc: feed
        private Dictionary<string, SyndicationFeed> feedsDictionary = new Dictionary<string, SyndicationFeed>();

        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;
        }

        public string Url
        {
            get => _url;
            set
            {
                _url = value;

                if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("Url"));
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        //tworzy nowy feed, dodaje go do listboxa i do listy
        private SyndicationFeed CreateNewFeed(string url, ListBox listBox, ObservableCollection<BlogPost> list)
        {
            try
            {
                var reader = XmlReader.Create(url);

                var feed = SyndicationFeed.Load(reader);


                foreach (var item in feed.Items)
                {
                    var blogpost = new BlogPost(item);
                    listBox.Items.Add(blogpost.Title);
                    list.Add(blogpost);
                }

                return feed;
            }
            catch (Exception e)
            {
                MessageBox.Show("URL is incorrect " + e.Message);
                return null;
            }
        }

        public void AddFeed(string url)
        {
            Url = url;

            var box = new ListBox
            {
                Name = $"box{_tabsCounter}"
            };

            //kolekcja potrzebna do SelectionChanged event
            var collection = new ObservableCollection<BlogPost>();
            var feed = CreateNewFeed(Url, box, collection);
            

            if (feed != null && !feedsDictionary.ContainsKey(url))
            {
                
                feedsDictionary.Add(url, feed);
                
                //nowy tabitem w tabcontrol
                var tabItem = new TabItem
                {
                    Header = feed.Title.Text +"(" + url + ")",
                    Name = $"tabItem{_tabsCounter}"
                };
                tabItem.Content = box;


                tabControl.Items.Add(tabItem);
                tabControl.SelectedItem = tabItem;
                _tabsCounter++;

                //czyszczenie textbox
                Url = string.Empty;

                box.SelectionChanged += (bsender, be) =>
                {
                    //databinding do linku pod tekstem
                    DataContext = box;

                    var tbox = bsender as ListBox;
                    var bp = collection[tbox.SelectedIndex];
                    hyperLink.NavigateUri = bp.Url;
                    bp.Body = bp.Body.Replace("_blank", "_self");
                    richTextBox.Document.Blocks.Clear();
                    richTextBox.AppendText(bp.Title);
                    richTextBox.AppendText(bp.Body);
                    webBrowser.NavigateToString("<meta http-equiv='Content-Type' content='text/html;charset=UTF-8'> " +
                                                bp.Body);
                };
            }
            else
            {
                MessageBox.Show("Feed is already opened or is invalid");
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            string[] linksCollection = ConfigurationManager.AppSettings.AllKeys;

            foreach (var key in linksCollection)
            {
                AddFeed(key);
            }

        }

        private void hyperLink_RequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            //Process.Start(e.Uri.ToString());
            webBrowser.Visibility = Visibility.Visible;
            webBrowser.Navigate(e.Uri);
        }


        private void webBrowser_Navigating(object sender, NavigatingCancelEventArgs e)
        {
        
            if (e.Uri != null && _navigation)
            {
                _navigation = false;
                webBrowser.Visibility = Visibility.Visible;
                webBrowser.Navigate(e.Uri);
            }
           
        }


        private void buttonAdd_Click(object sender, RoutedEventArgs e)
        {
            Url = textBoxUrl.Text;

            AddFeed(Url);
        }

        private void checkHTML_Unchecked(object sender, RoutedEventArgs e)
        {
            webBrowser.Visibility = Visibility.Hidden;
        }

        private void checkHTML_Checked(object sender, RoutedEventArgs e)
        {
            webBrowser.Visibility = Visibility.Visible;
            webBrowser.NavigateToString("<meta http-equiv='Content-Type' content='text/html;charset=UTF-8'> "
                                        + new TextRange(richTextBox.Document.ContentStart,
                                            richTextBox.Document.ContentEnd).Text);
        }

        private void ButtonClose_Click(object sender, RoutedEventArgs e)
        {
            var feedToRemove = feedsDictionary.Values.Single(x =>
                ((TabItem) tabControl.SelectedItem).Header.ToString().Contains(x.Links[0].Uri.ToString()));
            
            feedsDictionary.Remove(feedToRemove.Links[0].Uri.ToString());
            // usuwanie zaznaczonego tabitem
            tabControl.Items.Remove(tabControl.SelectedItem);
        }

        private void ButtonAddToFav_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                var settings = config.AppSettings.Settings;
                var item = tabControl.SelectedItem as TabItem;
                //wyłuskanie linku z headera
                var itemFeedLink = item.Header.ToString().Substring( item.Header.ToString().IndexOf("(")+1 ).Replace(")","");
                //MessageBox.Show(itemFeedLink);
                var feed = feedsDictionary[itemFeedLink];

                if (settings.AllKeys.Contains(itemFeedLink))
                {
                    MessageBox.Show("Feed is already added to favourites");
                    return;
                }

                settings.Add(itemFeedLink,null);
                config.Save(ConfigurationSaveMode.Full);
                ConfigurationManager.RefreshSection(config.AppSettings.Settings.ToString());
                MessageBox.Show($"Feed has been added to favs!\n{item.Header}\n{feed.Links[0].Uri}");
                
            }
            catch (ConfigurationErrorsException err)
            {
                MessageBox.Show("Error during adding to favourites" + err.Message);
            }
        }

        private void ButtonFav_Click(object sender, RoutedEventArgs e)
        {
            Window Favs = new WindowFavs(this);
            Favs.Owner = this;
            Favs.Show();
        }

        private void webBrowser_Navigated(object sender, NavigationEventArgs e)
        {
            _navigation = true;
        }
    }
}